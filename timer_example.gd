extends Panel


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal bat_signal


# Called when the node enters the scene tree for the first time.
func _ready():
	connect("bat_signal", self, "_on_bat_signal")
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Timer_timeout():
	emit_signal("bat_signal")


func _on_bat_signal():
	pass # Replace with function body.
	$Sprite.visible = ! $Sprite.visible
