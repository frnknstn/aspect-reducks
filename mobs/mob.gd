extends Node2D
class_name Mob

var Facing = GameDefs.Facing

enum {
	STATE_WALK,
	STATE_STAND,
	STATE_COMBAT,
	STATE_STORY,
	STATE_WAIT,
	STATE_DEAD,
	STATE_HURT,
}

signal died		# (self)


onready var sprite = $AnimatedSprite
var facing = Facing.RIGHT

var path_node: PathFollow2D
var speed: float
var health: int

var _state = STATE_WAIT


func set_state(state):
	# TODO: state change validation
	if state != _state:
		print("%s change state to %s" % [str(self), state])
		_state = state


func get_state():
	return _state


func walk(path: Path2D):
	"start us walking down a path"
	# clean up old node
	if path_node:
		path_node.queue_free()
	
	# create our follow node
	var path_follow = PathFollow2D.new()
	path_follow.loop = false
	path.add_child(path_follow)
	
	# walk this path
	self.position = path_follow.position
	$AnimationPlayer.play("walk")
	path_node = path_follow
	set_state(STATE_WALK)


func set_facing(f):
	# set our direction
	facing = f
	if f == GameDefs.Facing.RIGHT:
		sprite.scale = Vector2(1, 1)
	elif f == GameDefs.Facing.LEFT:
		sprite.scale = Vector2(-1, 1)

