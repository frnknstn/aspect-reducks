extends Mob


onready var FistShape = $AnimatedSprite/FistBox/FistShape2D

# Called when the node enters the scene tree for the first time.
func _ready():
	speed = 320.0
	start_idle()
	
func _unhandled_input(event):
	if get_state() == STATE_COMBAT:
		if event.is_action_pressed("gm_hit1"):
			attack()
			
	if get_state() in [STATE_COMBAT, STATE_HURT]:
		if event.is_action_pressed("gm_flip1"):
			if facing == Facing.LEFT:
				set_facing(Facing.RIGHT)
			else:
				set_facing(Facing.LEFT)


func _process(delta):
	if _state == STATE_WALK:
		process_walking(delta)


func process_walking(delta: float):
	# move us if we are on a path
	var curr_offset: float
	var frame_speed = delta * speed
	var end_node: PathNode
	
	if path_node:
		curr_offset = path_node.offset
		path_node.offset += frame_speed
		position = path_node.position
		
		if abs(path_node.rotation_degrees) < 90:
			set_facing(Facing.RIGHT)
		elif abs(path_node.rotation_degrees) > 90:
			set_facing(Facing.LEFT)
		
	# are we at the end of the path?
	if path_node.unit_offset == 1:
		end_node = path_node.get_parent().get_end_node()
		print("%s reached EndNode %s" % [str(self), str(end_node)])
		
		# implement a combat node for now
		if end_node is PathNodeCombat:
			set_state(STATE_COMBAT)
			$AnimationPlayer.play("stand")
			end_node.activate_node(self)
			end_node.connect("resolved", self, "_on_PathNodeCombat_resolved")
		else:
			# nothing to do?
			set_state(STATE_WAIT)


func step_animation(name: String, index: int):
	"Manually step through animation, for our AnimationPlayer to sync individual sprite frames"
	var sprite = $AnimatedSprite
	sprite.stop()
	sprite.set_animation(name)
	sprite.frame = index


func start_idle():
	FistShape.disabled = true
	$AnimationPlayer.play("stand")
	
func end_combat():
	assert(_state in [STATE_COMBAT, STATE_HURT])
	$FistCooldown.stop()
	$InvulnerableTimer.stop()
	start_idle()


func attack():
	if $FistCooldown.is_stopped():
		$FistCooldown.start()
		$AnimationPlayer.play("fist")

func _on_FistBox_area_entered(area: Area2D):
	"we fisted something"
	area.owner.hurt()
	area.owner.damage(50)
	

func hurt():
	"ow"
	if _state != STATE_COMBAT:
		# only get hurt while fighting
		return
	if not $InvulnerableTimer.is_stopped():
		return
	
	$AnimationPlayer.play("hurt")
	$InvulnerableTimer.start()
	set_state(STATE_HURT)

func recover():
	"I'm feeling better"
	assert(_state == STATE_HURT)
	
	set_state(STATE_COMBAT)
	$AnimationPlayer.play("stand")

func _on_PathNodeCombat_resolved(end_node):
	assert(_state in [STATE_COMBAT, STATE_HURT])
	
	end_combat()
	var next_walk_path = end_node.get_next_path()
	if next_walk_path:
		walk(next_walk_path)
	else:
		start_idle()
	
