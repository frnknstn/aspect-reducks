extends Mob

const ATTACK_RANGE = 31.5
const MIN_KNOCKBACK = 1.0

var target
var knockback = 0.0


func _ready():
	speed = 96.0
	health = 100
	$AnimationPlayer.play("stand")


func _process(delta):
	# billboard
	rotation = -get_parent().rotation
	
	# the animations use offset
	$AnimatedSprite/HitBox.position = $AnimatedSprite.offset
	
	# are we walking on a path?
	if path_node:
		# flip if moving left
		if abs(path_node.rotation_degrees) < 90:
			set_facing(Facing.RIGHT)
		elif abs(path_node.rotation_degrees) > 90:
			set_facing(Facing.LEFT)
		
		if knockback > 0:
			# are we being knocked back
			var frame_offset = path_node.offset - (knockback * 0.8 * delta)
			
			path_node.offset = frame_offset
			position = path_node.position
			
			# decay the knockback
			knockback = knockback - (delta * 75)
			if knockback < MIN_KNOCKBACK:
				knockback = 0.0
			
			
			if _state == STATE_DEAD:
				return

			# if we are knocked off our path or the target's path, die
			if frame_offset < 0:
				print("off our path")
				die()
				return
				
			# work out if this point is still on our target's path
			var nearest_target_path = path_node.get_parent().get_curve().get_closest_point(position)
			if (position - nearest_target_path).length() > 0.1:
				print("off target path: ", (position - nearest_target_path).length())
				die()
				return			
			
		elif _state == STATE_WALK:
			process_walking(delta)


func process_walking(delta: float):
	# move us if we are on a path
	var curr_offset = path_node.offset
	var frame_speed = delta * speed
	var target_offset: float
	
	# work out out distance to target
	target_offset = path_node.get_parent().get_curve().get_closest_offset(target.position) - ATTACK_RANGE
	
	if abs(target_offset - path_node.offset) > frame_speed:
		# can't reach target this frame
		path_node.offset += frame_speed
	else:
		# get into attack position
		path_node.offset = target_offset
		attack()
		
	position = path_node.position


func attack():
	set_state(STATE_COMBAT)	
	
	while true:
		$AnimationPlayer.play("attack")
		yield(get_tree().create_timer(3), "timeout")
		if _state != STATE_COMBAT:
			return


func hurt():
	"something has hurt this poor squirrel :("
	$AnimationPlayer.play("hurt")
	if _state != STATE_DEAD:
		set_state(STATE_HURT)
	
	# bring us down to ground
	var tween = $HurtTween
	var fall_time = $AnimatedSprite.offset.length() / 16 * 0.25
	tween.interpolate_property(
		$AnimatedSprite, "offset",
		$AnimatedSprite.offset, Vector2(0, 0),
		fall_time, Tween.TRANS_LINEAR, Tween.EASE_IN
	)
	tween.start()
	
	# TODO remove this shim code
	# really need to do actual animations system
	if _state != STATE_DEAD:
		yield(get_tree().create_timer(1), "timeout")
		if _state != STATE_DEAD:
			set_state(STATE_WALK)


func damage(dam):
	"take damage"
	if _state == STATE_DEAD:
		# this horse can't be beat
		return
		
	health -= dam
	knockback += dam
	print("%s damaged, health now %d" % [str(self), health])
	
	if health <= 0:
		die()


func die():
	"shuffle off"
	if _state == STATE_DEAD:
		return
	
	set_state(STATE_DEAD)
	$AnimatedSprite/HitBox/CollisionShape2D.disabled = true
	$AnimationPlayer.play("dead")
	
	yield(get_tree().create_timer(0.5), "timeout")
	
	for i in range(5):
		yield(get_tree().create_timer(0.2), "timeout")
		$AnimatedSprite.visible = false
		yield(get_tree().create_timer(0.2), "timeout")
		$AnimatedSprite.visible = true
	print("goodbye")
	emit_signal("died", self)
	queue_free()


func _on_HitBox_area_entered(area: Area2D):
	"we hit something"
	
	print(["squirrel hit", area.owner, area.owner.name, area.collision_layer])
	area.owner.hurt()


# override mob base
func set_state(new_state):
	
	# validate
	var change_ok = false	
	
	match _state:
		STATE_WAIT:
			change_ok = (new_state == STATE_WALK)
		STATE_WALK:
			change_ok = (new_state == STATE_STAND)
		STATE_STAND:
			change_ok = new_state in [STATE_WALK, STATE_COMBAT]
		STATE_COMBAT:
			change_ok = new_state in [STATE_STAND, STATE_HURT]
		STATE_HURT:
			change_ok = new_state in [STATE_WALK, STATE_DEAD]
		STATE_DEAD:
			change_ok = false
		
	if not change_ok:
		print_debug("Cannot change state from %s to %s" % [_state, new_state])
		# TODO: stop processing	
	
	.set_state(new_state)
