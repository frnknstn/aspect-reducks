extends ViewportContainer

onready var _viewport # = $Viewport
onready var _camera #= $Viewport/Camera

var _target: Node2D
var _size_change = null

# animation
var _size_goal: Vector2
var _size_goal_start: Vector2
var _positon_goal: Vector2
var _positon_goal_start: Vector2
var _goal_timer: float = 1.0
var _goal_rate: float = 1.0

func configure(primary:Viewport, position: Vector2, size: Vector2, target = null):
	# activate this viewport as a child of a primary
	_viewport.world_2d = primary.find_world_2d()

	_target = target
	set_position(position)
	
	# BUG: if I dynamically create a viewport with a non-zero X size, it acts wierd if I later resize
	# that viewport
	_set_port_size(Vector2(0, 1))
	_size_change = size		# set on first update
	
	# center camera maybe
	if target != null:
		_camera.offset = target.global_position
	else:
		_camera.offset = size / 2

func animate(position: Vector2, size: Vector2, rate: float = 1.0):
	"Lerp us to a new position and size"
	_goal_rate = rate
	_positon_goal_start = get_position()
	_positon_goal = position
	_goal_timer = 0.0
	
	if _size_change != null:
		_size_goal_start = _size_change
	else:
		_size_goal_start = get_size()
	_size_goal = size


func _set_port_size(size: Vector2):
	_viewport.set_size(size)
	set_size(size)

func _ready():
	_viewport = $Viewport
	_camera = $Viewport/Camera
	_camera.set_custom_viewport(_viewport)

func _process(delta):
	# queued resize
	if _size_change != null:
		_set_port_size(_size_change)
		_size_change = null
		
	# animate our viewport
	if _goal_timer < 1.0:
		_goal_timer += delta * _goal_rate
		if _goal_timer >= 1.0:
			_goal_timer = 1.0
		
		# ceil / floor to prevent visible seams
		var new_position = lerp(_positon_goal_start, _positon_goal, _goal_timer)
		new_position = Vector2(floor(new_position.x), floor(new_position.y))
		set_position(new_position)
		
		var new_size = lerp(_size_goal_start, _size_goal, _goal_timer)
		new_size = Vector2(ceil(new_size.x), ceil(new_size.y))
		_set_port_size(new_size)
		
	# center camera maybe
	if _target != null:
		_camera.offset = _target.global_position
