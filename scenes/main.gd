extends Node2D


var Squirrel = preload("res://mobs/squirrel.tscn")


func _ready():
	$Squirrel.set_facing(GameDefs.Facing.LEFT)
	
	$Player.walk($WalkPath)


func _process(delta):
	OS.set_window_title(GameDefs.TITLE + " - " + str(Engine.get_frames_per_second()) + " fps")

