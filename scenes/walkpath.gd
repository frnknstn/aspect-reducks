extends Path2D

class_name WalkPath

export(NodePath) var end_node


func get_end_node():
	"return the PathNode that is the terminator for this WalkPath"
	return get_node(end_node)
