extends Node2D

var AspectView = preload("res://scenes/aspectview.tscn")

onready var world = $WorldViewportContainer/WorldViewport/Main
var views = []


func _create_child_viewport(pos, size, target):
	print("New view: ", str([pos, size, str(target)]))
	var view = AspectView.instance()
	self.add_child(view, true)
	view.configure(
		$WorldViewportContainer/WorldViewport, 
		pos,
		size,
		target
	)
	
	views.append(view)
	return view

func add_viewport(target = null):
	"Add a new viewport to the right of our existing viewports"
	var view
	
	var view_count = len(views) + 1
	var window_size = get_viewport().size
	var port_size = Vector2(window_size.x / view_count, window_size.y)
	
	# resize the existing views
	for i in len(views):
		views[i].animate(Vector2(port_size.x * i, 0), port_size, 1)
		
	# add the new view
	if view_count == 1:
		# first viewport, no animation
		view = _create_child_viewport(Vector2(0, 0), port_size, target)
	else:
		view = _create_child_viewport(Vector2(window_size.x, 0), Vector2(0, port_size.y), target)
		view.animate(Vector2(port_size.x * (view_count - 1), 0), port_size, 1)

func _ready():
	$WorldViewportContainer.visible = false
	
	var target = world.get_node("Player")
	
	if true:
		# normal mode
		add_viewport(target)
	else:
		# demo mode
		demo(target)

func demo(target):
	var view_count = 8
	var x_offset = 0
	for i in range(view_count):
		add_viewport(target)

		yield(get_tree().create_timer(2), "timeout")

