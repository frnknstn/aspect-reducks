extends PathNode

"This path only leads to pain :("

class_name PathNodeCombat

signal resolved		# (self)

export(Array, NodePath) var spawn_paths

export(int) var spawn_count = 20
export(float) var spawn_interval = 3.4
var alive_count:int = 0


var Squirrel = preload("res://mobs/squirrel.tscn")


func activate_node(player):
	"A player has activated this node"
	
	var spawns = get_spawn_paths()
	
	while spawn_count > 0:
		for spawn in spawns:
			spawn_squirrel(spawn, player)
			spawn_count -= 1
			alive_count += 1
			
			if spawn_count <= 0:
				break
			
		yield(get_tree().create_timer(spawn_interval), "timeout")


func spawn_squirrel(spawn, target):
	"make a new squirrel friend"
	var mob = Squirrel.instance()

	spawn.add_child(mob)
	
	mob.target = target
	mob.walk(spawn)
	
	mob.connect("died", self, "_on_Mob_died")


func get_spawn_paths():
	"get the spawn path"
	var retval = []
	for path in spawn_paths:
		if path:
			retval.append(get_node(path))
	
	print(str(retval))
	return retval


func is_resolved():
	"did the player complete this combat node"
	return spawn_count == 0 and alive_count == 0

func _on_Mob_died(mob):
	self.alive_count -= 1
	print("%d/%d mobs remain in combat" % [alive_count, alive_count+spawn_count])
	if is_resolved():
		emit_signal("resolved", self)
