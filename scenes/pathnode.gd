extends Node2D

"An endpoint for a WalkPath"

class_name PathNode

export(NodePath) var _next_walk_path

func get_next_path():
	"overloadable func to return the next path"
	return get_node(_next_walk_path)
	
