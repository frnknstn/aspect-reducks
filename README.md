# Aspect Reducks #

A recreation of my previous unfinished project for Mini-LD 34 'Aspect'.

## Ideas #

* res 960x128
* Add ducks, not just squirrels
* combat is based on range (distance from player) and height
* move along Paths
* combat only while on fight nodes

## State plan: #

### Squirrels: #

Inputs:
* set_state()
* anim_end()

```
@startuml
wait --> walk
  walk --> stand
  stand --> walk
  stand --> combat
  combat --> stand
  combat --> hurt
  hurt --> walk
  hurt --> dead
@enduml
```
![](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuIfFp2nHqDMrKoXFpCdcKW1JO3vGHLuAK01CmrMW22JdvoObbW2580o4DbIXZD8Ye28GXR09cP-IcfZ2vP2Qbm9q3m00.png)

