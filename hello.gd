extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var primary_vp = $PrimaryViewContainer/PrimaryViewport
onready var secondary_vp = $SecondaryViewContainer/SecondaryViewport
onready var primary_cam = $PrimaryViewContainer/PrimaryViewport/PrimaryCam
onready var secondary_cam = $SecondaryViewContainer/SecondaryViewport/SecondaryCam


# Called when the node enters the scene tree for the first time.
func _ready():
	secondary_vp.world_2d = primary_vp.find_world_2d() #world_2d
	secondary_cam.set_custom_viewport(secondary_vp)
	
	primary_cam.set_custom_viewport(primary_vp)
	
	
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# show FPS
	OS.set_window_title(GameDefs.TITLE + " - " + str(Engine.get_frames_per_second()) + " fps")
